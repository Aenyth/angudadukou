import { Pipe, PipeTransform } from '@angular/core';
import {Place} from "./models/Place";

@Pipe({
  name: 'orderByPopularity'
})
export class OrderByPopularityPipe implements PipeTransform {

  transform(array: Array<Object>, args: string): Array<Object> {


    if (array == null) {
      return null;
    }

    array.sort((a: any, b: any) => {
      if (a.show[args] > b.show[args] ){
        return -1;
      }else if( a.show[args] < b.show[args] ){
        return 1;
      }else{
        return 0;
      }
    });

    return array;

  }

}
