import { Pipe, PipeTransform } from '@angular/core';
import {isString} from 'util';

@Pipe({
  name: 'split'
})
export class SplitPipe implements PipeTransform {

  private months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  transform (input: any, separator: string = ' ', value: string, limit?: number): any {

    if (!isString(input)) {
      return input;
    }

    switch (value){

      case 'day':
        return (input.split(separator, limit))[2].split('T', 1);

      case 'year':
        return (input.split(separator, limit))[0];

      case 'month':

        const month = (input.split(separator, limit))[1];
        let iModified = '0';

        for (let i = 1; i <= 12; i++){

          if (i < 10){

            iModified = '0' + i;

          }else{

            iModified = i + '';

          }

          if (month === iModified){

           return this.months[i];

          }

        }
        break;

      case 'time':
        return (input.split(separator, limit))[1];

      default :
        return null;
    }


  }

}
