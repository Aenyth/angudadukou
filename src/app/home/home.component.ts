import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Show} from '../models/Show';
import {ShowManagerService} from '../services/show-manager.service';
import {Place} from '../models/Place';
import {Room} from '../models/Room';
import {PlaceManagerService} from '../services/place-manager.service';
import {Router} from "@angular/router";
import {DataService} from "../services/data.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private test = '';

  private events = [];
  private places: Place[];
  private rooms = [];

  @Output() private placeChange: EventEmitter<Place> = new EventEmitter();

  constructor(
    public showService: ShowManagerService,
    public placeService: PlaceManagerService,
    public router : Router,
    public dataService : DataService) { }

  ngOnInit() {

    this.placeService.getAllPlaces().subscribe(
      places => this.places = Place.fromJsons(places),
      error => console.log(error),
      () => {

        for (let i = 0; i < this.places.length; i++){

          this.events[i] = this.places[i].show;
          this.rooms[i] = this.places[i].room;

        }});
  }

  public clickOnEvent(place : Place){

    this.dataService.placeSelected = place;
    this.router.navigate(['/info-event']);

  }

}
