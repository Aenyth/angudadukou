import { Component, OnInit } from '@angular/core';
import {PlaceManagerService} from '../services/place-manager.service';
import {Place} from '../models/Place';
import {DataService} from '../services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-autocomplete-component',
  templateUrl: './autocomplete-component.component.html',
  styleUrls: ['./autocomplete-component.component.css']
})
export class AutocompleteComponentComponent implements OnInit {

  private places: Place[];
  private index: number;
  public query= '';
  public strings = [];
  public filteredString = [];

  constructor(
    public placeService: PlaceManagerService,
    public router: Router,
    public dataService: DataService) {
    this.placeService.getAllPlaces().subscribe(
      places => this.places = Place.fromJsons(places),
      error => console.log(error),
      () => {

        for (let i = 0; i < this.places.length; i++){

          this.strings.push(this.places[i].show.name);

        }
      }
    );
  }

  ngOnInit() {
  }

  filter(){
    if (this.query !== ''){
      this.filteredString = this.strings.filter(function (e1) {
        return e1.toLowerCase().indexOf(this.query.toLowerCase()) > 1;
      }.bind(this));
    } else {
      this.filteredString = [];
    }
  }

  select(item){
    this.query = item;
    this.filteredString = [];
  }

  public clickOnEvent(place: Place){

    // We obtain the clicked string
    this.index = this.strings.indexOf(place);
    // Strings and place have the same index
    // So we can get the place element by its index
    this.dataService.placeSelected = this.places[this.index];
    this.router.navigate(['/info-event']);

  }

}
