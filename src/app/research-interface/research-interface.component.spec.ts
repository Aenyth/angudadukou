import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResearchInterfaceComponent } from './research-interface.component';

describe('ResearchInterfaceComponent', () => {
  let component: ResearchInterfaceComponent;
  let fixture: ComponentFixture<ResearchInterfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResearchInterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResearchInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
