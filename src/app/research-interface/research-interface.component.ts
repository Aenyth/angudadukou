import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from '../models/User';

@Component({
  selector: 'app-research-interface',
  templateUrl: './research-interface.component.html',
  styleUrls: ['./research-interface.component.css']
})
  export class ResearchInterfaceComponent implements OnInit {

  private menu1Visible = false;
  private menu2Visible = false;
  private menu3Visible = false;
  private menu4Visible = false;

  @Output() private typeChange: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  public showMenu(menu: number) {

    switch (menu){

      case 1:
        this.menu1Visible = true;
        this.menu2Visible = false;
        this.menu3Visible = false;
        this.menu4Visible = false;
        break;

      case 2:
        this.menu1Visible = false;
        this.menu2Visible = true;
        this.menu3Visible = false;
        this.menu4Visible = false;
        break;

      case 3:
        this.menu1Visible = false;
        this.menu2Visible = false;
        this.menu3Visible = true;
        this.menu4Visible = false;
        break;

      case 4:
        this.menu1Visible = false;
        this.menu2Visible = false;
        this.menu3Visible = false;
        this.menu4Visible = true;
        break;

      default :
        break;

    }

  }

  public sendType(type: string): void{


    this.typeChange.next(type);

  }

}
