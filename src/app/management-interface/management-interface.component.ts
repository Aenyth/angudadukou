import { Component, OnInit } from '@angular/core';
import {RoomManagerService} from '../services/room-manager.service';
import {Room} from '../models/Room';
import {ShowManagerService} from '../services/show-manager.service';
import {Show} from '../models/Show';
import {PlaceManagerService} from '../services/place-manager.service';
import {Place} from '../models/Place';

@Component({
  selector: 'app-management-interface',
  templateUrl: './management-interface.component.html',
  styleUrls: ['./management-interface.component.css']
})
export class ManagementInterfaceComponent implements OnInit {

  private settingsRoom = {
      delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      Id: {
        title: 'ID'
      },
      NbSeat: {
        title: 'Number of seat'
      },
      NameRoom: {
        title: 'Name'
      },
      Adress: {
        title: 'Adress'
      },
      ImagePath: {
        title: 'URL of the image'
      }
    }
  };

  private settingsEvent = {
  delete: {
    confirmDelete: true,
  },
  add: {
    confirmCreate: true,
  },
  edit: {
    confirmSave: true,
  },
  columns: {
    Id: {
      title: 'ID'
    },
    Popularity: {
      title: 'Popularity'
    },
    Name: {
      title: 'Name'
    },
    Date_Deb: {
      title: 'Date of the beginning'
    },
    Date_End: {
      title: 'Date of the end'
    },
    ImagePath: {
      title: 'URL of the image'
    },
    TypeShow: {
      title: 'Type',
      type: 'html',
      editor: {
        type: 'list',
        config: {
          list: [{ value: 'Rock', title: 'Rock' }, { value: 'R&B', title: 'R&B' }, { value: 'Alternative', title: 'Alternative' },
            { value: 'French', title: 'French' }, { value: 'Classic', title: 'Classic' }, { value: 'Rap', title: 'Rap' },
            { value: 'Metal', title: 'Metal' },{ value: 'Motor', title: 'Motor' },{ value: 'Boxe', title: 'Boxe' },
            { value: 'Football', title: 'Football' },{ value: 'Baseball', title: 'Baseball' },{ value: 'Tennis', title: 'Tennis' },
            { value: 'Comedy', title: 'Comedy' },{ value: 'Party', title: 'Party' },{ value: 'Theater', title: 'Theater' },
            { value: 'Dance', title: 'Dance' }, { value: 'Other', title: 'Other' }, { value: 'Family', title: 'Family' },
            { value: 'Kids', title: 'Kids' }, { value: 'Concert', title: 'Concert' },{
            value: '<b>Samantha</b>',
            title: 'Samantha'
          }]
        }
      }
    }
  }
};

  private settingsPlace = {
    delete: {
      confirmDelete: true,
    },
    add: {
      confirmCreate: true,
    },
    edit: {
      confirmSave: true,
    },
    columns: {
      Id: {
        title: 'ID'
      },
      Price: {
        title: 'Price'
      },
      Room: {
        title: 'Room ID',
      },
      Show: {
        title: 'Show ID'
      },
      Available: {
        title : 'Number of place available'
      }
    }
  };

  private dataRoom: any;
  private dataEvent: any;
  private dataPlace: any;

  constructor(public roomService: RoomManagerService, public showService: ShowManagerService, public placeService: PlaceManagerService) { }

  ngOnInit() {

    this.roomService.getAllRooms().subscribe(
      rooms => { this.dataRoom = rooms; });

    this.showService.getAllShows().subscribe(
      shows => {this.dataEvent = shows; });

    this.placeService.getAllPlaces().subscribe(
      places => {this.dataPlace = places; });

  }

  public onCreateConfirm(event): void {

    if (window.confirm('Are you sure you want to create?')) {

      event.confirm.resolve();

      const tmpRoom = new Room(event.newData['Id'], event.newData['NbSeat'], event.newData['NameRoom'], event.newData['Adress'], event.newData['ImagePath']);

      this.roomService.createRoom(tmpRoom).subscribe(room => this.dataRoom.push(room));
    } else {
      event.confirm.reject();
    }

  }

  public onCreateConfirmE(event): void {

    if (window.confirm('Are you sure you want to create?')) {

      event.confirm.resolve();

      console.log(event);

      const tmpShow = new Show(event.newData['Id'], event.newData['Popularity'], event.newData['Name'],
        event.newData['Date_Deb'], event.newData['Date_End'], event.newData['ImagePath'], event.newData['TypeShow']);

      this.showService.createShow(tmpShow).subscribe(show => this.dataEvent.push(show));
    } else {
      event.confirm.reject();
    }

  }

  public onCreateConfirmP(event): void {

    if (window.confirm('Are you sure you want to create?')) {

      console.log(this.findRoom(event.newData['IdRoom']));

      event.confirm.resolve();

      const tmpPlace = new Place(event.newData['Id'], event.newData['Price'], this.findRoom(event.newData['Room']),
        this.findEvent(event.newData['Show']), event.newData['Available']);

      this.placeService.createPlace(tmpPlace).subscribe(place => this.dataPlace.push(place));
    } else {
      event.confirm.reject();
    }

  }

  public onSaveConfirm(event): void {

    if (window.confirm('Are you sure you want to save?')) {

      event.confirm.resolve();

      const tmpRoom = new Room(event.newData['Id'], event.newData['NbSeat'], event.newData['NameRoom'], event.newData['Adress'], event.newData['ImagePath']);

      this.roomService.updateRoom(tmpRoom).subscribe();

    } else {
      event.confirm.reject();
    }

  }

  public onSaveConfirmE(event): void {

    if (window.confirm('Are you sure you want to save?')) {

      event.confirm.resolve();

      const tmpShow = new Show(event.newData['Id'], event.newData['Popularity'], event.newData['Name'],
        event.newData['Date_Deb'], event.newData['Date_End'], event.newData['ImagePath'], event.newData['TypeShow']);

      this.showService.updateShow(tmpShow).subscribe();

    } else {
      event.confirm.reject();
    }

  }

  public onSaveConfirmP(event): void {

    if (window.confirm('Are you sure you want to save?')) {

      event.confirm.resolve();

      const tmpPlace = new Place(event.newData['Id'], event.newData['Price'], this.findRoom(event.newData['Room']),
        this.findEvent(event.newData['Show']), event.newData['Available']);

      this.placeService.updatePlace(tmpPlace).subscribe();

    } else {
      event.confirm.reject();
    }

  }

  public onDeleteConfirm(event): void {

    if (window.confirm('Are you sure you want to delete?')) {

      event.confirm.resolve();

      const index = event.data['Id'];

      const DELETE_ROOM = () => this.dataRoom.splice(index, 1);
      const DISPLAY_ERROR = (error) => console.error(error);

      this.roomService.deleteRoom(event.data['Id']).subscribe(DELETE_ROOM, DISPLAY_ERROR);

    } else {
      event.confirm.reject();
    }

  }

  public onDeleteConfirmE(event): void {

    if (window.confirm('Are you sure you want to delete?')) {

      event.confirm.resolve();

      const index = event.data['Id'];

      const DELETE_SHOW = () => this.dataEvent.splice(index, 1);
      const DISPLAY_ERROR = (error) => console.error(error);

      this.showService.deleteShow(event.data['Id']).subscribe(DELETE_SHOW, DISPLAY_ERROR);

    } else {
      event.confirm.reject();
    }

  }

  public onDeleteConfirmP(event): void {

    if (window.confirm('Are you sure you want to delete?')) {

      event.confirm.resolve();

      const index = event.data['Id'];

      const DELETE_PLACE = () => this.dataPlace.splice(index, 1);
      const DISPLAY_ERROR = (error) => console.error(error);

      this.placeService.deletePlace(event.data['Id']).subscribe(DELETE_PLACE, DISPLAY_ERROR);

    } else {
      event.confirm.reject();
    }

  }

  public findEvent(id: number): Show{

    for (let i = 0; i < this.dataEvent.length; i++){

      if (this.dataEvent[i].Id == id){

        return this.dataEvent[i];

      }

    }

}
  public findRoom(id: number): Room{

    for (let i = 0; i < this.dataRoom.length; i++){

      if (this.dataRoom[i].Id == id){

        return this.dataRoom[i];

      }

    }

  }


}
