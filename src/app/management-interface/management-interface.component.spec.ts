import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementInterfaceComponent } from './management-interface.component';

describe('ManagementInterfaceComponent', () => {
  let component: ManagementInterfaceComponent;
  let fixture: ComponentFixture<ManagementInterfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementInterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
