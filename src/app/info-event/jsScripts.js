$(document).ready(function() {
    // By default, we hide the content.
    $("#realContent").hide();;

    /* If we click on the top button, we go to the information and make it visible.*/
    $("#button").click(function () {

        $("#show_name_effect").hide("slow");
        $('html, body').animate({
            scrollTop: $("#content").offset().top

        }, 2000, null, function(){
            // Callback function
            $("#realContent").show("50");
            $("#toTop").show(200);
        });
    })

    /* If we click on the arrow, we go back to the poster. We hide the content */
    $("#toTop").click(function(){
        $("#toTop").hide(10);
        $("#realContent").hide(10);
        $("#show_name_effect").show("slow");
        $('html,body').animate({
            scrollTop:$("#button").offset().top
        },2000,null,function(){
        });
    })

});