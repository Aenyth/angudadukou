import {Component, Input, OnInit} from '@angular/core';
import {Place} from '../models/Place';
import {Show} from '../models/Show';
import {Room} from '../models/Room';
import {BookingManagerService} from '../services/booking-manager.service';
import {Booking} from '../models/Booking';
import {User} from '../models/User';
import {ActivatedRoute, Router} from '@angular/router';
import {DataService} from '../services/data.service';

@Component({
  selector: 'app-info-event',
  templateUrl: './info-event.component.html',
  styleUrls: ['./info-event.component.css']
})
export class InfoEventComponent implements OnInit {

  Arr = Array; //Array type captured in a variable

  private nbTicket = 0;
  private bill = 0;
  private isComplet = false;

  constructor(public dataService: DataService,
              public router: Router) { }

  ngOnInit() {

    this.isComplet = (this.dataService.placeSelected.available == 0);

  }

  public getBill(){

    this.bill = this.nbTicket * this.dataService.placeSelected.price;

  }

  public createBooking(){

    if (this.nbTicket == 0){

      window.alert('Please enter a number of ticket');

    }else{

    this.getBill();

    const tmpPlace = new Place(this.dataService.placeSelected.id, this.dataService.placeSelected.price, this.dataService.placeSelected.room, this.dataService.placeSelected.show, this.dataService.placeSelected.available);
    const tmpBooking = new Booking(0, this.nbTicket, this.bill, tmpPlace, this.dataService.userConnected.id);

    this.dataService.invoice = tmpBooking;

    this.router.navigate(['/ticket']);

  }}

}
