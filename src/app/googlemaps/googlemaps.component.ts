import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { MouseEvent } from '@agm/core';

@Component({
  selector: 'app-googlemaps',
  templateUrl: './googlemaps.component.html',
  styleUrls: ['./googlemaps.component.css']
})
export class GooglemapsComponent implements OnInit {
  // google maps zoom level
  zoom: number = 8;

  // initial center position for the map
  lat: number =  50.8504500;
  lng: number =  4.3487800;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  mapClicked($event: MouseEvent) {
    this.markers.push({
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      draggable: true
    });
  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log('dragEnd', m, $event);
  }

  markers: marker[] = [
    {
      lat: 50.8504500,
      lng:  4.3487800,
      label: 'Bruxelles, Rue de la Paille, 9',
      draggable: true
    },
    {
      lat: 50.467388,
      lng: 4.871985,
      label: 'Namur, Rue de Namur, 40',
      draggable: false
    },
    {
      lat: 50.6325574,
      lng: 5.5796662,
      label: 'Liège, Rue de Bruxelles, 14',
      draggable: true
    }
  ]
  constructor(public router: Router) { }

  ngOnInit() {
  }
}

// just an interface for type safety.
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
