import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {User} from '../models/User';
import {UserManagerService} from '../services/user-manager.service';
import {DataService} from '../services/data.service';
import {Route, Router} from "@angular/router";

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  private tmpName = '';
  private tmpFstName = '';
  private tmpBirth = null;
  private tmpLogin = '';
  private tmpPassword = '';
  private tmpStreet = '';
  private tmpNumber = 0;
  private tmpZipCode = 0;
  private tmpCity = '';
  private tmpMail = '';
  private tmpTel = '';
  private tmpLanguage = '';
  private tmpTitle = '';
  private tmpCountry = '';
  private tmpIsAdmin = false;

  private tmpUser: User;

  private languages  = ['Français', 'English'];
  private titles = ['Mr', 'Ms'];

  @Output() private userChange: EventEmitter<User> = new EventEmitter();

  constructor(public userService: UserManagerService, public dataService: DataService, public router : Router) { }

  ngOnInit() {
  }

  public createUser(): void  {

       this.tmpUser = new User(this.tmpName, this.tmpFstName, this.tmpBirth, this.tmpLogin, this.tmpPassword, this.tmpStreet,
       this.tmpNumber, this.tmpZipCode, this.tmpCity, this.tmpMail, this.tmpTel, this.tmpLanguage, this.tmpTitle,
       this.tmpCountry);
       this.userService.createUser(this.tmpUser).subscribe(user => this.tmpUser.id = User.fromJson(user).id);
       this.dataService.isUserConnected = true;
       this.dataService.userConnected = this.tmpUser;
       this.router.navigate(['/']);
  }


}
