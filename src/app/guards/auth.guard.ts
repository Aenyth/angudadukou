import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate() {

    if (localStorage.getItem('currentUser')){

      return true;

    }

    window.alert('You need to be connected to do that ! ');
    this.router.navigate(['/user-connection']);
    return false;

  }
}
