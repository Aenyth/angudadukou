import { Component, OnInit } from '@angular/core';
import {RoomManagerService} from '../services/room-manager.service';
import {Room} from '../models/Room';
import {DataService} from '../services/data.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-room-interface',
  templateUrl: './room-interface.component.html',
  styleUrls: ['./room-interface.component.css']
})
export class RoomInterfaceComponent implements OnInit {

  private rooms: Room[] = [];

  constructor(public roomService: RoomManagerService,
              public dataService: DataService,
              public router: Router) { }

  ngOnInit() {

    this.roomService.getAllRooms().subscribe(rooms => this.rooms = Room.fromJsons(rooms));

  }

  public clickOnRoom(room: Room){

    this.dataService.roomSelected = room;
    this.router.navigate(['/info-room']);

  }

}
