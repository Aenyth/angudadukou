import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomInterfaceComponent } from './room-interface.component';

describe('RoomInterfaceComponent', () => {
  let component: RoomInterfaceComponent;
  let fixture: ComponentFixture<RoomInterfaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomInterfaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
