import {RouterModule, Routes} from "@angular/router";
import {UserConnectionComponent} from "./user-connection/user-connection.component";
import {HomeComponent} from "./home/home.component";
import {AuthGuard} from "./guards/auth.guard";
import {InfoEventComponent} from "./info-event/info-event.component";
import {ManagementInterfaceComponent} from "./management-interface/management-interface.component";
import {UserFormComponent} from "./user-form/user-form.component";
import {ResearchComponent} from "./research/research.component";
import {RoomInterfaceComponent} from "./room-interface/room-interface.component";
import {UserInterfaceComponent} from "./user-interface/user-interface.component";
import {TicketComponent} from "./ticket/ticket.component";
import {InfoRoomComponent} from "./info-room/info-room.component";
import {GooglemapsComponent} from "./googlemaps/googlemaps.component";


const appRoutes: Routes = [
  { path: 'user-connection', component: UserConnectionComponent },
  { path: 'info-event', component : InfoEventComponent},
  { path: 'user-form', component : UserFormComponent},
  { path: 'research', component : ResearchComponent},
  { path: 'room', component : RoomInterfaceComponent},
  { path: 'info-room', component : InfoRoomComponent},
  { path: 'user-interface', component : UserInterfaceComponent},
  { path: 'ticket', component : TicketComponent, canActivate: [AuthGuard]},
  { path: 'admin', component : ManagementInterfaceComponent},
  { path: '', component: HomeComponent},
  { path: 'shops', component: GooglemapsComponent},

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
