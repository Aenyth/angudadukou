import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../models/User';
import {AuthentificationService} from '../services/authentification.service';
import {Router} from '@angular/router';
import {DataService} from "../services/data.service";
import {UserManagerService} from "../services/user-manager.service";

@Component({
  selector: 'app-user-connection',
  templateUrl: './user-connection.component.html',
  styleUrls: ['./user-connection.component.css']
})
export class UserConnectionComponent implements OnInit {

  private tmpLogin = '';
  private tmpPassword = '';

  private tmpUser: User;

  @Input() private _isHomeComponentVisible: boolean;
  @Input() private _isUserConnectionVisible: boolean;
  @Output() private userChange: EventEmitter<User> = new EventEmitter();

  constructor(public authentificationService: AuthentificationService,
              public router: Router,
              public dataService: DataService,
              public userService: UserManagerService) { }

  ngOnInit() {
  }

  public getUser(): void {

    this.userService.getUser(this.tmpLogin, this.tmpPassword).subscribe(
      user => this.tmpUser = User.fromJson(user));

    this.authentificationService.loginDB(this.tmpLogin, this.tmpPassword).subscribe(result => {



      if (result === true) {

        this.dataService.isUserConnected = true;
        this.dataService.userConnected = this.tmpUser;

        if (JSON.parse(localStorage.getItem('currentUser')).token.role === 'admin'){


          this.router.navigate(['/admin']);

        }else{

          this.router.navigate(['/']);

        }

      }else{

        this.dataService.userConnected = new User('', '', null, '', '', '', 0, 0, '', '', '', '', '', '');
        window.alert("Access denied");

      }

    });


  }

}
