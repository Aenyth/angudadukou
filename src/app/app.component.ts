import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from './models/User';
import {Place} from './models/Place';
import {DataService} from './services/data.service';
import {Room} from './models/Room';
import {Show} from './models/Show';
import {AuthentificationService} from './services/authentification.service';
import {UserManagerService} from './services/user-manager.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  private _isUserFormVisible = false;
  private _isHomeCompVisible = true;
  private _isUserConnectionVisible = false;
  private _isAdminInterfaceVisible = false;
  private _isInfoEventVisible = false;
  private _isResearchVisible = false;
  private _isRoomVisible = false;
  private _isUserVisible = false;

  private _isUserConnected = this.dataService.isUserConnected;

  private wordAccount = 'My account';

  private userConnected: User = this.dataService.userConnected;
  private eventSelected: Place;

  constructor(public dataService: DataService,
              public userService: UserManagerService,
              public authentificationService: AuthentificationService,
              public router: Router){}

  ngOnInit() {

    this.dataService.placeSelected = new Place(0, 0, new Room(0, 0, '', '', ''), new Show(0, 0, '', null, null, '', ''), 0);
    this.dataService.userConnected = new User('', '', null, '', '', '', 0, 0, '', '', '', '', '', '');

    if (localStorage.getItem('currentUser')) {

      this.userService.getUserLogin(JSON.parse(localStorage.getItem('currentUser')).token.unique_name).subscribe(
        data => this.dataService.userConnected = User.fromJson(data)
      );

    }

  }


  public logOut(){

    this.authentificationService.logout();
    this.router.navigate(['/']);

  }

}
