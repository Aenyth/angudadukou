import {Component, Input, OnInit} from '@angular/core';
import {Place} from '../models/Place';
import {Show} from '../models/Show';
import {Room} from '../models/Room';
import {DataService} from '../services/data.service';
import {BookingManagerService} from '../services/booking-manager.service';
import {Booking} from '../models/Booking';
import {Router} from '@angular/router';
import {PlaceManagerService} from "../services/place-manager.service";

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})
export class TicketComponent implements OnInit {

  private  url = 'user-interface';

  constructor(private dataService: DataService,
              private bookingService: BookingManagerService,
              private placeService : PlaceManagerService) { }

  ngOnInit() {


  }

  validate(){

    this.bookingService.createBooking(this.dataService.invoice).subscribe();
    this.dataService.placeSelected.available -= this.dataService.invoice.nbPlace;
    this.placeService.updatePlace(this.dataService.placeSelected).subscribe();
    this.dataService.invoice = null;

  }

}
