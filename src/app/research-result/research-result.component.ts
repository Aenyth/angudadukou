import {Component, Input, OnInit} from '@angular/core';
import {Place} from '../models/Place';

@Component({
  selector: 'app-research-result',
  templateUrl: './research-result.component.html',
  styleUrls: ['./research-result.component.css']
})
export class ResearchResultComponent implements OnInit {

  @Input() public placesResult: Place[] = [];

  constructor() {
  }

  ngOnInit() {


  }

  public test() {

    console.log(this.placesResult);

  }

}
