import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';


import {AlertModule} from 'ngx-bootstrap';


import {Ng2SmartTableModule} from 'ng2-smart-table';


import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {UserFormComponent} from './user-form/user-form.component';
import {UserManagerService} from './services/user-manager.service';
import {HttpClientModule} from '@angular/common/http';
import {UserConnectionComponent} from './user-connection/user-connection.component';
import {ManagementInterfaceComponent} from './management-interface/management-interface.component';
import {RoomManagerService} from './services/room-manager.service';
import {ShowManagerService} from './services/show-manager.service';
import {PlaceManagerService} from './services/place-manager.service';
import {InfoEventComponent} from './info-event/info-event.component';
import {TicketComponent} from './ticket/ticket.component';
import {UserInterfaceComponent} from './user-interface/user-interface.component';
import {RoomInterfaceComponent} from './room-interface/room-interface.component';
import {ResearchInterfaceComponent} from './research-interface/research-interface.component';
import {ResearchResultComponent} from './research-result/research-result.component';
import {ResearchComponent} from './research/research.component';
import {InfoRoomComponent} from './info-room/info-room.component';
import {BookingManagerService} from './services/booking-manager.service';
import {Routes} from '@angular/router';
import {routing} from "./app.routing";
import {AuthGuard} from "./guards/auth.guard";
import {AuthentificationService} from "./services/authentification.service";
import {DataService} from "./services/data.service";
import { OrderByPopularityPipe } from './order-by-popularity.pipe';
import { RedirectionDirective } from './redirection.directive';
import { SplitPipe } from './split.pipe';
import {AutocompleteComponentComponent} from "./autocomplete-component/autocomplete-component.component";

import { AgmCoreModule } from '@agm/core';
import { GooglemapsComponent } from './googlemaps/googlemaps.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserFormComponent,
    UserConnectionComponent,
    ManagementInterfaceComponent,
    InfoEventComponent,
    TicketComponent,
    UserInterfaceComponent,
    RoomInterfaceComponent,
    ResearchInterfaceComponent,
    ResearchResultComponent,
    ResearchComponent,
    InfoRoomComponent,
    OrderByPopularityPipe,
    RedirectionDirective,
    SplitPipe,
    AutocompleteComponentComponent,
    GooglemapsComponent
  ],
  imports: [
    BrowserModule,
    AlertModule.forRoot(),
    Ng2SmartTableModule,
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDbne0NQKNh08ngRJbHAFFiHjBbOPt__XY'
    }),
    HttpClientModule,
    routing

  ],
  providers: [UserManagerService, RoomManagerService, ShowManagerService,
    PlaceManagerService, BookingManagerService, AuthGuard, AuthentificationService, DataService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
