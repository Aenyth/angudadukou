import {Show} from "./Show";
import {Room} from "./Room";
import {Place} from "./Place";

export class Booking{

  private _id : number;
  private _nbPlace : number;
  private _price : number;
  private _idPlace : Place;
  private _idUser : number;

  constructor(id: number, nbPlace: number, price: number, idPlace: Place, idUser: number) {
    this._id = id;
    this._nbPlace = nbPlace;
    this._price = price;
    this._idPlace = idPlace;
    this._idUser = idUser;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get nbPlace(): number {
    return this._nbPlace;
  }

  set nbPlace(value: number) {
    this._nbPlace = value;
  }

  get price(): number {
    return this._price;
  }

  set price(value: number) {
    this._price = value;
  }

  get idPlace(): Place {
    return this._idPlace;
  }

  set idPlace(value: Place) {
    this._idPlace = value;
  }

  get idUser(): number {
    return this._idUser;
  }

  set idUser(value: number) {
    this._idUser = value;
  }

  public cleanDataForSending(): any {

    return {

      'Id' : this.id,
      'NbPlace' : this.nbPlace,
      'Price' : this.price,
      'Place': this.idPlace.cleanDataForSending(),
      'IdUser': this.idUser

    };

  }

  public static fromJson(rawBooking: any): Booking {

    return new Booking(rawBooking['Id'], rawBooking['NbPlace'], rawBooking['Price'], Place.fromJson(rawBooking['Place']), rawBooking['IdUser']);

  }

  public static fromJsons(rawBookings: any[]): Booking[]{

    return rawBookings.map(Booking.fromJson);

  }

}
