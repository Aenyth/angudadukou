export class Show {

  private _id: number;
  private _popularity: number;
  private _name: string;
  private _dateBeg: Date;
  private _dateEnd: Date;
  private _imagePath: string;
  private _typeShow: string;


  constructor(id: number, popularity: number, name: string, dateBeg: Date, dateEnd: Date, imagePath: string, typeShow: string) {
    this._id = id;
    this._popularity = popularity;
    this._name = name;
    this._dateBeg = dateBeg;
    this._dateEnd = dateEnd;
    this._imagePath = imagePath;
    this._typeShow = typeShow;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get popularity(): number {
    return this._popularity;
  }

  set popularity(value: number) {
    this._popularity = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get dateBeg(): Date {
    return this._dateBeg;
  }

  set dateBeg(value: Date) {
    this._dateBeg = value;
  }

  get dateEnd(): Date {
    return this._dateEnd;
  }

  set dateEnd(value: Date) {
    this._dateEnd = value;
  }

  get imagePath(): string {
    return this._imagePath;
  }

  set imagePath(value: string) {
    this._imagePath = value;
  }

  get typeShow(): string {
    return this._typeShow;
  }

  set typeShow(value: string) {
    this._typeShow = value;
  }

  public cleanDataForSending(): any{

    return {

      'Id' : this.id,
      'Popularity' : this.popularity,
      'Name' : this.name,
      'Date_Deb' : this.dateBeg,
      'Date_End' : this.dateEnd,
      'ImagePath': this.imagePath,
      'TypeShow' : this.typeShow

    };

  }

  public static fromJson(rawShow: any): Show {

    return new Show(rawShow['Id'], rawShow['Popularity'], rawShow['Name'], rawShow['Date_Deb'], rawShow['Date_End'], rawShow['ImagePath'], rawShow['TypeShow']);

  }

  public static fromJsons(rawShows: any[]): Show[]{

    return rawShows.map(Show.fromJson);

  }

}
