export class Room {

  private _id: number;
  private _nbSeat: number;
  private _nameRoom: string;
  private _adress: string;
  private _imagePath : string;


  constructor(id: number, nbSeat: number, nameRoom: string, adress: string, imagePath: string) {
    this._id = id;
    this._nbSeat = nbSeat;
    this._nameRoom = nameRoom;
    this._adress = adress;
    this._imagePath = imagePath;
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get nbSeat(): number {
    return this._nbSeat;
  }

  set nbSeat(value: number) {
    this._nbSeat = value;
  }

  get nameRoom(): string {
    return this._nameRoom;
  }

  set nameRoom(value: string) {
    this._nameRoom = value;
  }

  get adress(): string {
    return this._adress;
  }

  set adress(value: string) {
    this._adress = value;
  }

  get imagePath(): string {
    return this._imagePath;
  }

  set imagePath(value: string) {
    this._imagePath = value;
  }

  public cleanDataForSending(): any {

    return {

      'Id' : this.id,
      'NbSeat' : this.nbSeat,
      'NameRoom' : this.nameRoom,
      'Adress' : this.adress,
      'ImagePath' : this.imagePath

    };

  }

  public static fromJson(rawRoom: any): Room {

    return new Room(rawRoom['Id'], rawRoom['NbSeat'], rawRoom['NameRoom'], rawRoom['Adress'], rawRoom['ImagePath']);

  }

  public static fromJsons(rawRooms: any[]): Room[]{

    return rawRooms.map(Room.fromJson);

  }

}
