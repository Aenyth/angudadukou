import {Room} from './Room';
import {Show} from './Show';

export class Place {

  private _id: number;
  private _price: number;
  private _room: Room;
  private _show: Show;
  private _available : number;


  constructor(id: number, price: number, room: Room, show: Show, available: number) {
    this._id = id;
    this._price = price;
    this._room = room;
    this._show = show;
    this._available = available;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get price(): number {
    return this._price;
  }

  set price(value: number) {
    this._price = value;
  }

  get room(): Room {
    return this._room;
  }

  set room(value: Room) {
    this._room = value;
  }

  get show(): Show {
    return this._show;
  }

  set show(value: Show) {
    this._show = value;
  }

  get available(): number {
    return this._available;
  }

  set available(value: number) {
    this._available = value;
  }

  public cleanDataForSending(): any {

    return {

      'Id' : this.id,
      'Price' : this.price,
      'Room' : this.room.cleanDataForSending(),
      'Show' : this.show.cleanDataForSending(),
      'Available': this.available

    };

  }

  public static fromJson(rawPlace: any): Place {

    return new Place(rawPlace['Id'], rawPlace['Price'], Room.fromJson(rawPlace['Room']), Show.fromJson(rawPlace['Show']), rawPlace['Available']);

  }

  public static fromJsons(rawPlaces: any[]): Place[]{

    return rawPlaces.map(Place.fromJson);

  }

}
