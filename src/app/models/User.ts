export class User {

  private _id: number;
  private _name: string;
  private _fstname: string;
  private _birth: Date;
  private _login: string;
  private _password: string;
  private _street: string;
  private _number: number;
  private _zipcode: number;
  private _city: string;
  private _mail: string;
  private _phone: string;
  private _language: string;
  private _title: string;
  private _country: string;
  private _isAdmin: boolean;


  constructor(name: string, fstname: string, birth: Date, login: string, password: string, street: string,
              number: number, zipcode: number, city: string, mail: string, phone: string, language: string,
              title: string, country: string) {
    this._name = name;
    this._fstname = fstname;
    this._birth = birth;
    this._login = login;
    this._password = password;
    this._street = street;
    this._number = number;
    this._zipcode = zipcode;
    this._city = city;
    this._mail = mail;
    this._phone = phone;
    this._language = language;
    this._title = title;
    this._country = country;
  }


  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get fstname(): string {
    return this._fstname;
  }

  set fstname(value: string) {
    this._fstname = value;
  }

  get birth(): Date {
    return this._birth;
  }

  set birth(value: Date) {
    this._birth = value;
  }

  get login(): string {
    return this._login;
  }

  set login(value: string) {
    this._login = value;
  }

  get password(): string {
    return this._password;
  }

  set password(value: string) {
    this._password = value;
  }

  get street(): string {
    return this._street;
  }

  set street(value: string) {
    this._street = value;
  }

  get number(): number {
    return this._number;
  }

  set number(value: number) {
    this._number = value;
  }

  get zipcode(): number {
    return this._zipcode;
  }

  set zipcode(value: number) {
    this._zipcode = value;
  }

  get city(): string {
    return this._city;
  }

  set city(value: string) {
    this._city = value;
  }

  get mail(): string {
    return this._mail;
  }

  set mail(value: string) {
    this._mail = value;
  }

  get phone(): string {
    return this._phone;
  }

  set phone(value: string) {
    this._phone = value;
  }

  get language(): string {
    return this._language;
  }

  set language(value: string) {
    this._language = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get country(): string {
    return this._country;
  }

  set country(value: string) {
    this._country = value;
  }

  get isAdmin(): boolean {
    return this._isAdmin;
  }

  set isAdmin(value: boolean) {
    this._isAdmin = value;
  }

  public cleanDataForSending(): any {

    return {

        'Id' : this.id,
        'Name' : this.name,
        'Fstname' : this.fstname,
        'Birth' : this.birth,
        'Login' : this.login,
        'Password' : this.password,
        'Street' : this.street,
        'Number' : this.number,
        'Zipcode' : this.zipcode,
        'City' : this.city,
        'Mail' : this.mail,
        'Tel' : this.phone,
        'Language' : this.language,
        'Title' : this.title,
        'Country' : this.country,
        'IsAdmin' : this.isAdmin

    };

  }

   public static fromJson(rawUser: any): User {

    if(rawUser != null) {

      const tmpUser = new User(rawUser['Name'], rawUser['Fstname'], rawUser['Birth'], rawUser['Login'], rawUser['Password'],
        rawUser['Street'], rawUser['Number'], rawUser['Zipcode'], rawUser['City'], rawUser['Mail'], rawUser['Tel'], rawUser['Language'],
        rawUser['Title'], rawUser['Country']);

      tmpUser._id = rawUser['Id'];
      tmpUser._isAdmin = rawUser['IsAdmin'];

      return tmpUser;

    }

    return null;

  }



}
