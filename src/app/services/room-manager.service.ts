import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Room} from '../models/Room';

@Injectable()
export class RoomManagerService {

  public roomObs: Observable<Room> = null;
  public rooms : Observable<Room[]> = null;

  constructor(public http: HttpClient) { }

  public getAllRooms(): Observable<Room[]> {

    this.rooms =  this.http.get <Room[]>('http://localhost:4200/api/room');
    return this.rooms;
  }

  public createRoom(room: Room): Observable<Room> {

    this.roomObs = this.http.post <Room>('http://localhost:4200/api/room', room.cleanDataForSending());
    return this.roomObs;
  }

  public deleteRoom(id: number): Observable<any> {

    return this.http.delete('http://localhost:4200/api/room', {

      params : new HttpParams().set('id', id + '')

    });

  }

  public updateRoom(room: Room): Observable<Room> {

    this.roomObs =  this.http.put <Room>('http://localhost:4200/api/room', room.cleanDataForSending());
    return this.roomObs;
  }



}
