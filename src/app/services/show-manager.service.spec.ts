import { TestBed, inject } from '@angular/core/testing';

import { ShowManagerService } from './show-manager.service';

describe('ShowManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowManagerService]
    });
  });

  it('should be created', inject([ShowManagerService], (service: ShowManagerService) => {
    expect(service).toBeTruthy();
  }));
});
