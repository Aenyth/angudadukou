import { Injectable } from '@angular/core';
import {User} from '../models/User';
import {Place} from '../models/Place';
import {Booking} from '../models/Booking';
import {Room} from "../models/Room";

@Injectable()
export class DataService {

  userConnected: User;
  isUserConnected = !(localStorage.getItem('currentUser') === null);
  placeSelected: Place;
  invoice: Booking;
  roomSelected : Room;

  constructor() { }

}
