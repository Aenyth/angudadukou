import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Show} from '../models/Show';

@Injectable()
export class ShowManagerService {

  public showObs: Observable<Show> = null;
  public shows: Observable<Show[]> = null;
  constructor(public http: HttpClient) { }

  public getAllShows(): Observable<Show[]> {

    this.shows = this.http.get <Show[]>('http://localhost:4200/api/event');

    return this.shows;
  }

  public createShow(show: Show): Observable<Show> {

    this.showObs = this.http.post <Show>('http://localhost:4200/api/event', show.cleanDataForSending());
    return this.showObs;
  }

  public deleteShow(id: number): Observable<any> {

    return this.http.delete('http://localhost:4200/api/event', {

      params : new HttpParams().set('id', id + '')

    });

  }

  public updateShow(show: Show): Observable<Show> {

    this.showObs = this.http.put <Show>('http://localhost:4200/api/event', show.cleanDataForSending());
    return this.showObs;
  }

}
