import { TestBed, inject } from '@angular/core/testing';

import { RoomManagerService } from './room-manager.service';

describe('RoomManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoomManagerService]
    });
  });

  it('should be created', inject([RoomManagerService], (service: RoomManagerService) => {
    expect(service).toBeTruthy();
  }));
});
