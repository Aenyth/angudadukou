import { TestBed, inject } from '@angular/core/testing';

import { PlaceManagerService } from './place-manager.service';

describe('PlaceManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PlaceManagerService]
    });
  });

  it('should be created', inject([PlaceManagerService], (service: PlaceManagerService) => {
    expect(service).toBeTruthy();
  }));
});
