import { Injectable } from '@angular/core';
import {User} from '../models/User';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Show} from '../models/Show';

@Injectable()
export class UserManagerService {

  public userObs: Observable<User> = null;
  public users: Observable<User[]> = null;

  constructor(public http: HttpClient) { }

  public createUser(user: User): Observable<User> {


    this.userObs = this.http.post <User>('http://localhost:4200/api/user',
      user.cleanDataForSending());

   return this.userObs;
  }

  public getAllUsers(): Observable<User[]> {

      this.users = this.http.get <User[]>('http://localhost:4200/api/user');
      return this.users;

  }

  public getUser(login: string, password: string): Observable <User> {

    let Params = new HttpParams();

    Params = Params.append('login', login + '');
    Params = Params.append('password', password + '');

    this.userObs = this.http.get <User>('http://localhost:4200/api/user', {

      params : Params

    });

  return this.userObs;

  }

  public getUserLogin(login: string): Observable <User> {

    let Params = new HttpParams();

    Params = Params.append('login', login + '');

    this.userObs = this.http.get <User>('http://localhost:4200/api/user', {

      params : Params

    });

    return this.userObs;

  }

  public updateUser(user: User): Observable<User> {

    this.userObs = this.http.put <User>('http://localhost:4200/api/user', user.cleanDataForSending());

    return this.userObs;

  }

}
