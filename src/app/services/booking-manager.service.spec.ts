import { TestBed, inject } from '@angular/core/testing';

import { BookingManagerService } from './booking-manager.service';

describe('BookingManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BookingManagerService]
    });
  });

  it('should be created', inject([BookingManagerService], (service: BookingManagerService) => {
    expect(service).toBeTruthy();
  }));
});
