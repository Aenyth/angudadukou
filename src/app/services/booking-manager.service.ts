import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Booking} from '../models/Booking';

@Injectable()
export class BookingManagerService {

  public bookingObs: Observable<Booking> = null;

  public bookings: Observable<Booking[]> = null;

  constructor(public http: HttpClient) { }

  public getAllBookings(): Observable<Booking[]> {

    this.bookings = this.http.get <Booking[]>('http://localhost:4200/api/booking');
    return this.bookings;
  }

  public getUserBookings(id: number): Observable<Booking[]> {

    this.bookings = this.http.get <Booking[]>('http://localhost:4200/api/booking', {

      params : new HttpParams().set('id', id + '')

    });
    return this.bookings;
  }

  public createBooking(booking: Booking): Observable<Booking> {

    this.bookingObs = this.http.post <Booking>('http://localhost:4200/api/booking', booking.cleanDataForSending());
    return this.bookingObs;
  }

  public deleteBooking(id: number): Observable<any> {

    return this.http.delete('http://localhost:4200/api/booking', {

      params : new HttpParams().set('id', id + '')

    });

  }

  public updateBooking(booking: Booking): Observable<Booking> {

    this.bookingObs = this.http.put <Booking>('http://localhost:4200/api/booking', booking.cleanDataForSending());
    return this.bookingObs;
  }

}
