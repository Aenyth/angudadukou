import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import {JwtHelper} from '../util/JwtHelper';
import {HttpClient, HttpParams} from "@angular/common/http";
import {DataService} from "./data.service";

@Injectable()
export class AuthentificationService {

  public token: string;

  constructor(private http: HttpClient, private dataService : DataService) {
    // set token if saved in local storage
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  public loginDB(username: string, password: string): Observable<boolean> {

    let Params = new HttpParams();

    Params = Params.append('username', username + '');
    Params = Params.append('password', password + '');

    return this.http.get('http://localhost:4200/api/token/', {params : Params})
      .map(response => {
        // login successful if there's a jwt token in the response

        if(response === "Unauthorized"){

          return false;

        }

        const jwtHelper = new JwtHelper();
        const token = jwtHelper.decodeToken(response.toString());

        if (token) {
          // set token property
          this.token = token;

          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));

          // return true to indicate successful login
          return true;
        } else {
          // return false to indicate failed login
          return false;
        }
      });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
    this.dataService.isUserConnected = false;
  }


}
