import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Place} from '../models/Place';

@Injectable()
export class PlaceManagerService {

  public placeObs: Observable<Place> = null;

  public places: Observable<Place[]> = null;

  constructor(public http: HttpClient) { }

  public getAllPlaces(): Observable<Place[]> {

    this.places = this.http.get <Place[]>('http://localhost:4200/api/place');
    return this.places;
  }

  public getPlacesType(type: string): Observable<Place[]> {

    this.places = this.http.get <Place[]>('http://localhost:4200/api/place', {

      params : new HttpParams().set('type', type + '')

    });

    return this.places;

  }

  public createPlace(place: Place): Observable<Place> {

    this.placeObs = this.http.post <Place>('http://localhost:4200/api/place', place.cleanDataForSending());
    return this.placeObs;
  }

  public deletePlace(id: number): Observable<any> {

    return this.http.delete('http://localhost:4200/api/place', {

      params : new HttpParams().set('id', id + '')

    });

  }

  public updatePlace(place: Place): Observable<Place> {

    this.placeObs = this.http.put <Place>('http://localhost:4200/api/place', place.cleanDataForSending());
    return this.placeObs;
  }

}
