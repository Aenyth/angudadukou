import {Directive, HostListener, Input} from '@angular/core';
import {Router} from "@angular/router";

@Directive({
  selector: '[appRedirection]'
})
export class RedirectionDirective {

  constructor(private router : Router){}

  @Input('appRedirection') route: string;

  @Input() confirmMessage = 'Are you sure you want to do this?';

  @HostListener('click', ['$event'])
  confirmFirst() {
    const confirmed = window.confirm(this.confirmMessage);

    console.log('confirm was', confirmed);

    if (confirmed) {

      this.router.navigate(['/' + this.route]);

    }else{

      this.router.navigate(['/']);

    }
  }

}
