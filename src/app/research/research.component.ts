import { Component, OnInit } from '@angular/core';
import {PlaceManagerService} from "../services/place-manager.service";
import {Place} from "../models/Place";

@Component({
  selector: 'app-research',
  templateUrl: './research.component.html',
  styleUrls: ['./research.component.css']
})
export class ResearchComponent implements OnInit {


  private isInterfaceVisible = true;
  private isResultVisible = false;

  public places : Place[] = [];

  constructor(public placeService: PlaceManagerService) { }

  ngOnInit() {
  }

  public getResult(type: string): void{

    this.isInterfaceVisible = false;
    this.isResultVisible = true;

    this.placeService.getPlacesType(type).subscribe(
      places => this.places = Place.fromJsons(places));


  }

}
