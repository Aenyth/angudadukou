import {Component, Input, OnInit} from '@angular/core';
import {User} from '../models/User';
import {UserManagerService} from "../services/user-manager.service";
import {DataService} from "../services/data.service";
import {Booking} from "../models/Booking";
import {BookingManagerService} from "../services/booking-manager.service";

@Component({
  selector: 'app-user-interface',
  templateUrl: './user-interface.component.html',
  styleUrls: ['./user-interface.component.css']
})
export class UserInterfaceComponent implements OnInit {

  private user: User;
  private bookings: Booking[] = [];

  private languages  = ['Français', 'English'];
  private titles = ['Mr', 'Ms'];

  constructor(private userService: UserManagerService, private dataService: DataService,
              private bookingService: BookingManagerService) { }

  ngOnInit() {

    this.user = this.dataService.userConnected;
    this.bookingService.getUserBookings(this.user.id).subscribe(bookings => this.bookings = Booking.fromJsons(bookings));

  }

  public updateUser(){

    this.userService.updateUser(this.user).subscribe();
    this.dataService.userConnected = this.user;

  }

}
